package com.serhii.karavatskiy.cleanapp.di

import com.serhii.karavatskiy.cleanapp.data.ServiceApi
import com.serhii.karavatskiy.cleanapp.data.repository.AuthRepositoryImpl
import com.serhii.karavatskiy.cleanapp.domain.repository.AuthRepository
import com.serhii.karavatskiy.cleanapp.domain.usecase.AuthUseCase
import com.serhii.karavatskiy.cleanapp.domain.usecase.impl.AuthUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun providesRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://www.google.com.ua").build()
    }

    @Provides
    fun providesServiceApi(retrofit: Retrofit): ServiceApi {
        return retrofit.create(ServiceApi::class.java)
    }

    @Provides
    fun providesAuthRepository(serviceApi: ServiceApi): AuthRepository =
        AuthRepositoryImpl(serviceApi)

    @Provides
    fun providesAuthUseCase(authRepository: AuthRepository): AuthUseCase {
        return AuthUseCaseImpl(authRepository)
    }
}