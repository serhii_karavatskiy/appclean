package com.serhii.karavatskiy.cleanapp.presentation.extensions

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 11:45 AM
 */
fun AppCompatEditText.value(): String {
    return this.text.toString()
}

fun AppCompatActivity.showToast(value: String) {
    Toast.makeText(this, value, Toast.LENGTH_SHORT).show()
}