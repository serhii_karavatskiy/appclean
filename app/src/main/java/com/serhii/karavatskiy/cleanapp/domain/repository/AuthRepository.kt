package com.serhii.karavatskiy.cleanapp.domain.repository

import com.serhii.karavatskiy.cleanapp.domain.common.ResultState

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 9:14 AM
 */
interface AuthRepository {
    suspend fun loginIn(userName:String, email: String): ResultState<Boolean>

    fun isSessionValid(): Boolean
}