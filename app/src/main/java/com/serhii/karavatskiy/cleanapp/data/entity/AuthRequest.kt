package com.serhii.karavatskiy.cleanapp.data.entity

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 11:28 AM
 */
data class AuthRequest(val userName: String, val userEmail: String)