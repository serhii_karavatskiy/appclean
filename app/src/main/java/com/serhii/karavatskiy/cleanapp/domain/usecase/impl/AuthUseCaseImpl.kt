package com.serhii.karavatskiy.cleanapp.domain.usecase.impl

import com.serhii.karavatskiy.cleanapp.domain.common.ResultState
import com.serhii.karavatskiy.cleanapp.domain.repository.AuthRepository
import com.serhii.karavatskiy.cleanapp.domain.usecase.AuthUseCase

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 9:33 AM
 */
class AuthUseCaseImpl(private val authRepository: AuthRepository) : AuthUseCase {

    override suspend fun loginIn(userName: String, userEmail: String): ResultState<Boolean> {
        return authRepository.loginIn(userName, userEmail)
    }
}