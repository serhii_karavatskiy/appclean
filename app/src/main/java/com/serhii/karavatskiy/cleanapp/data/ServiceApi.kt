package com.serhii.karavatskiy.cleanapp.data

import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.*

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 11:08 AM
 */
interface ServiceApi {

    @POST("/login")
    suspend fun loginIn(
        @Path("userName") userName: String,
        @Path("email") email: String
    ): Response<String> {
        return Response.success(UUID.randomUUID().toString())
    }
}