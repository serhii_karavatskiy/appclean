package com.serhii.karavatskiy.cleanapp.presentation.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.serhii.karavatskiy.cleanapp.domain.common.ResultState
import com.serhii.karavatskiy.cleanapp.domain.usecase.AuthUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 9:27 AM
 */
@HiltViewModel
class AuthViewModel @Inject constructor(private val authUseCase: AuthUseCase) : ViewModel() {
    private val isLoggedIn by lazy { MutableLiveData<ResultState<Boolean>>() }

    fun isLoggedIn(): LiveData<ResultState<Boolean>> = isLoggedIn

    fun processLogin(userName: String, email: String) {
        viewModelScope.launch {
            isLoggedIn.postValue(authUseCase.loginIn(userName, email))
        }
    }
}