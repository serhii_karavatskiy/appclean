package com.serhii.karavatskiy.cleanapp.presentation.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ActivityNavigator
import com.serhii.karavatskiy.cleanapp.databinding.ActivityMainBinding
import com.serhii.karavatskiy.cleanapp.domain.common.ResultState
import com.serhii.karavatskiy.cleanapp.presentation.extensions.showToast
import com.serhii.karavatskiy.cleanapp.presentation.extensions.value
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LaunchActivity : AppCompatActivity() {

    private val viewModel: AuthViewModel by viewModels()
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            updateButtonState()
        }

        override fun afterTextChanged(p0: Editable?) {
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setObserver()
        setViews()
    }

    private fun setObserver() {
        viewModel.isLoggedIn().observe(this, { result ->
            when (result) {
                is ResultState.Success -> {
                    navigateInsideApp()
                }
                is ResultState.Error -> {
                    //error showing if need
                    showToast(result.error)
                }
                is ResultState.Exception -> {
                    //error showing if need
                }
            }
        })
    }

    private fun updateButtonState() {
        binding.buttonLogin.isEnabled =
            binding.inputUserName.value().isNotEmpty() && binding.inputEmail.value().isNotEmpty()
    }

    private fun navigateInsideApp() {
        ActivityNavigator(this).apply {
            this.navigate(
                this.createDestination().setIntent(
                    Intent(
                        this@LaunchActivity, HomeScreenActivity::class.java
                    )
                ), null, null, null
            )
        }
    }

    private fun setViews() {
        updateButtonState()
        binding.buttonLogin.setOnClickListener {
            viewModel.processLogin(binding.inputUserName.value(), binding.inputEmail.value())
        }

        binding.inputUserName.addTextChangedListener(textWatcher)
        binding.inputEmail.addTextChangedListener(textWatcher)
    }
}