package com.serhii.karavatskiy.cleanapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 8:51 AM
 */
@HiltAndroidApp
class CleanAppApplication : Application()