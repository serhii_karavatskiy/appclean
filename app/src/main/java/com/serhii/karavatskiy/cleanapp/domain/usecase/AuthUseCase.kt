package com.serhii.karavatskiy.cleanapp.domain.usecase

import com.serhii.karavatskiy.cleanapp.domain.common.ResultState

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 9:13 AM
 */
interface AuthUseCase {
    suspend fun loginIn(userName: String, userEmail: String): ResultState<Boolean>
}