package com.serhii.karavatskiy.cleanapp.data.repository

import com.serhii.karavatskiy.cleanapp.data.ServiceApi
import com.serhii.karavatskiy.cleanapp.domain.common.ResultState
import com.serhii.karavatskiy.cleanapp.domain.repository.AuthRepository

/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 9:24 AM
 */
class AuthRepositoryImpl(private val serviceApi: ServiceApi) : AuthRepository {

    private var sessionId: String? = null

    override suspend fun loginIn(userName: String, email: String): ResultState<Boolean> {

        return if (isSessionValid()) {
            ResultState.Success(true)
        } else {
            try {
                val result = serviceApi.loginIn(userName, email)
                if (result.isSuccessful) {
                    ResultState.Success(true)
                } else {
                    ResultState.Error(result.message(), false)
                }
            } catch (e: Exception) {
                //mocked result
                return ResultState.Success(true)
            }
        }
    }

    override fun isSessionValid(): Boolean {
        return sessionId != null
    }

}