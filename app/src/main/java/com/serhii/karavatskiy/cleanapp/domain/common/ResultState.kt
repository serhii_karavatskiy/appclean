package com.serhii.karavatskiy.cleanapp.domain.common
/**
 * Created by Serhii Karavatskiy
 * Date: 4/30/21
 * Time: 9:20 AM
 */
sealed class ResultState<T> {

    data class Success<T>(val data: T) : ResultState<T>()

    data class Error<T>(val error: String, val data: T?) : ResultState<T>()

    data class Exception<T>(val error: String): ResultState<T>()
}
